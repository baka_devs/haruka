package utils.reposter;

import core.Haruka;
import static core.MessageSender.sender;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.PhotoSize;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import utils.general.Crutches;
import utils.inlines.Inlines;
import utils.sql.DatabaseManagement;


public class Reposter {
    private static ArrayList<String> chatsToRepost;
    private static String finalDest;
    public static synchronized void checkAndRepost(Message message) {
	if(chatsToRepost.contains(String.valueOf(message.getChatId()))
            && !Haruka.currentConfig.getBotUserNamesList().contains(message.getFrom().getUserName())){
	    repost(message);
	}
    }
    
    public static void init(){
        chatsToRepost = Haruka.currentConfig.getChatsToRepostFromList();
        finalDest = Haruka.currentConfig.getReposterFinalDestId();
    }    
    
    private static void repostToFinalDest(String photo, String caption){
        SendPhoto sendPhoto = new SendPhoto();
        sendPhoto.setPhoto(photo);
        sendPhoto.setChatId(finalDest);
        sendPhoto.setCaption(caption);
        sendPhotoMessage(sendPhoto);
    }
    
    private static void deleteMessage(int msgId, long chatId){
        try {
            DefaultBotOptions options = new DefaultBotOptions();
            DeleteMessage deleteMessage = new DeleteMessage(chatId, msgId);
            sender = new DefaultAbsSender(options) {
                @Override
                public String getBotToken() {
                    return Haruka.HarukaBot.getBotToken();
                }
            };
            sender.execute(deleteMessage);
        } catch (TelegramApiException ex) {
            Logger.getLogger(Reposter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static synchronized void parseCommand(String command, long chatId, int msgId) {
    String commands[] = command.split(" ");
        switch (commands[1]) {
            case "✔":
                repostToFinalDest(commands[0], DatabaseManagement.getCaptionByMessage(msgId,chatId));
                deleteMessage(msgId, chatId);
                break;
            case "❌":
                deleteMessage(msgId, chatId);
                break;
            default:
                Haruka.output("Wrong inline input!\n Got: " + commands[0]);
        }
    }
    private static void repost(Message message){
        List<PhotoSize> photos = message.getPhoto();
        PhotoSize photoToRepost = photos.get(photos.size()-1);
        SendPhoto sendPhoto = new SendPhoto();
        sendPhoto.setPhoto(photoToRepost.getFileId());
        sendPhoto.setChatId(Haruka.currentConfig.getReposterControlChatId());
        sendPhoto = Inlines.attachInlinesToPhotoMessage(sendPhoto.getPhoto(), sendPhoto, createInlineButtonNames());
        String caption = "";
        if(!Crutches.tgGetUser(message).equals("UNKNOWN USER")){
            caption = "By: "+Crutches.tgGetUserWithoutUsername(message);
        }
        sendPhoto.setCaption(caption+"\nFrom: "+Crutches.tgGetChat(message));
        sendPhotoMessage(sendPhoto);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Reposter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void sendPhotoMessage(SendPhoto message){
        DefaultBotOptions options = new DefaultBotOptions();
        sender = new DefaultAbsSender(options) {
            @Override
            public String getBotToken() {
                return Haruka.HarukaBot.getBotToken();
            }
        };
        try {
            Message sentMessage = sender.sendPhoto(message);
            Haruka.messageOutput(sentMessage);
            if(!sentMessage.isChannelMessage())
            DatabaseManagement.addMessage(sentMessage);
        } catch (TelegramApiException | IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private static ArrayList<String> createInlineButtonNames(){
        ArrayList<String> list = new ArrayList<>();
        list.add("✔");
        list.add("❌");
        return list;
    }
}
