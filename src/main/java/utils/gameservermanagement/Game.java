package utils.gameservermanagement;

import org.telegram.telegrambots.exceptions.TelegramApiException;
import utils.inlines.Inlines;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Game {
    String name;
    private ArrayList<String> maps = new ArrayList<>();
    private ArrayList<String> menuitems = new ArrayList<>();

    private void init() {
        menuitems.add("start");
        menuitems.add("shutdown");
        menuitems.add("changemap");
        Crutches.serverManagementOutput("Initializing " + this.name + "...");
        getMaps();
        Crutches.serverManagementOutput("Game " + this.name + " has " + maps.size() + " maps.");
    }

    private void getMaps() {
        try {
            maps = Crutches.getMapsFromMapFile(this.name);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void parseCommand(String command, long chatId, int msgId) {
        //Crutches.srvmngOutput("Game "+this.name+" got this: "+trimCommand(command));
        String commands[] = trimCommand(command).split(" ");
        switch (commands[0]) {
            case "map":
                if (!commands[1].equals("back")) {
                    changeMap(commands[1]);
                } else showInlineMenu(chatId, msgId);
                break;
            case "back":
                Management.manageEdited(chatId, msgId);
                break;
            case "changemap":
                showChangeMapMenu(chatId, msgId);
                break;
            case "management":
                showInlineMenu(chatId, msgId);
                break;
            case "start":
                start();
                break;
            case "shutdown":
                shutdown();
                break;
            default:
                break;
        }
    }

    private String trimCommand(String command) {
        command = command.replace(this.name, "");
        command = command.trim();
        return command;
    }

    private void changeMap(String map) {
        Crutches.serverManagementOutput("Server " + this.name + " is changing map to " + map);
        callScript("map " + map);
    }

    private void shutdown() {
        Crutches.serverManagementOutput("Server " + this.name + " is shutting down");
        callScript("stop");
    }

    private void start() {
        Crutches.serverManagementOutput("Server " + this.name + " is starting");
        callScript("start");
    }

    private void showInlineMenu(long chatId, int msgId) {
        try {
            Inlines.createAndEditInlines(chatId, menuitems, this.name, msgId);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void showChangeMapMenu(long chatId, int msgId) {
        try {
            Inlines.createAndEditInlines(chatId, maps, this.name + " map", msgId);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void callScript(String command) {
        try {
            //Crutches.srvmngOutput("Executing: "+new File(".").getAbsolutePath()+"scripts/"+this.name+".sh "+command);
            utils.general.Crutches.checkAndCreateDirectories(new File(".").getAbsolutePath() + "/config/gamescripts/");
            Runtime.getRuntime().exec(new File(".").getAbsolutePath() + "/config/gamescripts/" + this.name + ".sh " + command);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected Game(String gamename) {
        name = gamename;
        init();
    }
}
