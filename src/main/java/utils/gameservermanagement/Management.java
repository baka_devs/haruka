package utils.gameservermanagement;

import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import utils.inlines.Inlines;

import java.util.ArrayList;

public class Management {
    private static ArrayList<Game> games = new ArrayList<>();

    public static void initialize() {
        games.add(new Game("quake3"));
        games.add(new Game("cs1.6"));
    }

    public static void manage(Message message) {
        try {
            Inlines.createAndSendInlines(message.getChatId(), getGameNames(), "management");
        } catch (TelegramApiException ignored) {
        }
    }

    static void manageEdited(long chatId, int msgId) {
        try {
            Inlines.createAndEditInlines(chatId, getGameNames(), "management", msgId);
        } catch (TelegramApiException ignored) {
        }
    }

    public static Game getGameByName(String name) {
        for (Game game : games) {
            if (game.name.equals(name)) {
                return game;
            }
        }
        return null;
    }

    private static ArrayList<String> getGameNames() {
        ArrayList<String> gameNameList = new ArrayList<>();
        for (Game game : games) {
            gameNameList.add(game.name);
        }
        return gameNameList;
    }
}
