package utils.sql;

import core.Haruka;
import core.MessageSender;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import utils.general.Crutches;
import utils.updateresolvers.ReadableUpdateResolver;
import utils.updateresolvers.UpdateResolver;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.*;

public class DatabaseManagement {
    public static void addMessageByUpdate(Update update) throws IOException {
        addMessageToLogsDB(update.getMessage(), update.getMessage().getChatId(),
                update.getMessage().getFrom().getId(),
                Crutches.tgGetUserWithoutUsername(update.getMessage()),
                ReadableUpdateResolver.resolveMessages(update.getMessage(), true, false),
                update.getMessage().getMessageId(),
                new Timestamp(update.getMessage().getDate().longValue() * 1000L),
                Crutches.tgGetChat(update.getMessage()),
                UpdateResolver.resolveMessages(update.getMessage(), false, false),
                Crutches.tgGetChatType(update.getMessage()),
                "Haruka v." + Haruka.version, Haruka.currentConfig.getLogSqlTableName());
    }

    public static void addMessage(Message message) throws IOException {
        addMessageToLogsDB(message, message.getChatId(),
                message.getFrom().getId(),
                Crutches.tgGetUserWithoutUsername(message),
                ReadableUpdateResolver.resolveMessages(message, true, false),
                Long.valueOf(message.getMessageId()),
                new Timestamp(message.getDate().longValue() * 1000L),
                Crutches.tgGetChat(message),
                UpdateResolver.resolveMessages(message, false, false),
                Crutches.tgGetChatType(message),
                "Haruka v." + Haruka.version, Haruka.currentConfig.getLogSqlTableName());
    }

    private static void addMessageToLogsDB(Message msgObj, long ChatId, int UserId, String Username,
                                           String Message, long MsgId, Timestamp time_stamp, String ChatName,
                                           String message_info, String chat_type, String bot, String table) {
        try {
            String query = "insert into "+table+"(chat_id, user_id, user_name, message_text, msg_id, time_stamp,"
                    + " chat_name, message_info, chat_type, bot)"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = SQLConnectionsHandler.getLogDBConnection().prepareStatement(query);
            preparedStmt.setLong(1, ChatId);
            preparedStmt.setInt(2, UserId);
            preparedStmt.setString(3, Username);
            preparedStmt.setString(4, Message);
            preparedStmt.setLong(5, MsgId);
            preparedStmt.setTimestamp(6, time_stamp);
            preparedStmt.setString(7, ChatName);
            preparedStmt.setString(8, message_info);
            preparedStmt.setString(9, chat_type);
            preparedStmt.setString(10, bot);
            Haruka.dbgOutput("EXECUTING QUERY: " +preparedStmt.toString());
            preparedStmt.execute();
        } catch (SQLIntegrityConstraintViolationException e) {
            Haruka.output("Duplicate message. This message is already written.");
        } catch (SQLException e) {
            if(e.getMessage().contains("doesn't exist")){
                configureDatabase(msgObj);
                return;
            }
            SQLConnectionsHandler.reconnectLogDB();
            e.printStackTrace();
        }
    }

    public static String[] addQuotationToQuotationsDB(int UserId, String Username, String citation, Timestamp time_stamp, String table) {
        try {
            String query = "insert into "+table+" (citation, date, user_id, user_name)"
                    + " values (?, ?, ?, ?)";
            PreparedStatement preparedStmt = SQLConnectionsHandler.getCitationsDBConnection().prepareStatement(query);
            preparedStmt.setString(1, citation);
            preparedStmt.setTimestamp(2, time_stamp);
            preparedStmt.setInt(3, UserId);
            preparedStmt.setString(4, Username);
            Haruka.dbgOutput("EXECUTING QUERY: " +preparedStmt.toString());
            preparedStmt.execute();
        } catch (SQLIntegrityConstraintViolationException e) {
            System.out.println("Could not write the citation. SQL exception.");
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String stackTrace = sw.toString();
            return new String[] {"FAIL", stackTrace};
        } catch (SQLException e) {
            SQLConnectionsHandler.reconnectCitationsDB();
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String stackTrace = sw.toString();
            return new String[] {"FAIL", stackTrace};
        }
        return new String[] {"SUCCESS"};
    }

    public static void getUserStatistics(Message msg) throws SQLException {
        long ChatId = msg.getChatId();
        long UserId;
        if (msg.isReply()) {
            UserId = msg.getReplyToMessage().getFrom().getId();
        } else {
            UserId = msg.getFrom().getId();
        }
        String message;
        String query = "SELECT count(*) FROM aka_log WHERE user_id='";
        query += String.valueOf(UserId) + "' AND chat_id='";
        query += String.valueOf(ChatId) + "';";
        PreparedStatement stmnt = SQLConnectionsHandler.getLogDBConnection().prepareStatement(query);
        Haruka.dbgOutput("EXECUTING QUERY: " + query);
        ResultSet rs = stmnt.executeQuery(query);
        rs.first();
        message = "Номер партбилета: " + String.valueOf(UserId) + "\n";
        message += ("Задокументировано ");
        message += rs.getString("count(*)");
        query = "SELECT * FROM aka_log WHERE user_id='";
        query += String.valueOf(UserId) + "' AND chat_id='";
        query += String.valueOf(ChatId) + "' ORDER BY time_stamp ASC;";
        stmnt = SQLConnectionsHandler.getLogDBConnection().prepareStatement(query);
        Haruka.dbgOutput("EXECUTING QUERY: " + query);
        rs = stmnt.executeQuery(query);
        rs.first();
        message += (" щитпостов c " + rs.getDate("time_stamp"));
        MessageSender.sendMessage(ChatId, message);
    }

    public static String getQuotationsByUser(Message message){
        StringBuilder output = new StringBuilder("Цитаты " + Crutches.tgGetUserWithoutUsername(message) + ":");
        try {
            String query = "SELECT * FROM "+Haruka.currentConfig.getCitationsSqlTableName()+" WHERE " +
                    "user_id="+message.getReplyToMessage().getFrom().getId();
            Statement stmnt = SQLConnectionsHandler.getCitationsDBConnection().createStatement();
            Haruka.dbgOutput("EXECUTING QUERY: " + query);
            ResultSet rs = stmnt.executeQuery(query);

            if(!rs.first()) return "No quotations from this user!";
            do {
                output.append("\n").append(rs.getString("citation")).append(" (").append(rs.getDate("date")).append(").");
            } while (rs.next());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(output.length() == 0) return "No quotations from this user!";
        return output.toString();
    }

    public static String getCaptionByMessage(int msgId, long chatId){
        String query = "SELECT message_info FROM "+Haruka.currentConfig.getLogSqlTableName()+" WHERE " +
                "chat_id="+chatId+" AND msg_id="+msgId;
        Statement stmnt;
        try {
        stmnt = SQLConnectionsHandler.getLogDBConnection().createStatement();
        Haruka.dbgOutput("EXECUTING QUERY: " + query);
        ResultSet rs = stmnt.executeQuery(query);
        if(!rs.first()) {
            Haruka.output("ERROR");
            return null;
        }
        return rs.getString("message_info").split("Caption:")[1];
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void configureDatabase(Message message){
        Haruka.output("Log database is not configured for Haruka.\n" +
                "Trying to configure...");
    String databaseConfiguringSQLQuery = "CREATE TABLE `"+Haruka.currentConfig.getLogSqlTableName()+"` (\n" +
            " `user_id` bigint(20) NOT NULL,\n" +
            " `user_name` text COLLATE utf8mb4_bin,\n" +
            " `chat_id` bigint(20) NOT NULL,\n" +
            " `chat_type` text CHARACTER SET utf8,\n" +
            " `chat_name` text CHARACTER SET utf8,\n" +
            " `time_stamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
            " `message_text` text COLLATE utf8mb4_bin,\n" +
            " `bot` text CHARACTER SET utf8,\n" +
            " `msg_id` bigint(20) NOT NULL,\n" +
            " `message_info` text COLLATE utf8mb4_bin,\n" +
            " `id` int(11) NOT NULL AUTO_INCREMENT,\n" +
            " UNIQUE KEY `chat_msg_id` (`chat_id`,`msg_id`) USING BTREE,\n" +
            " UNIQUE KEY `id` (`id`) USING BTREE,\n" +
            " KEY `time_stamp` (`time_stamp`),\n" +
            " KEY `chat_id` (`chat_id`),\n" +
            " KEY `user_id` (`user_id`)\n" +
            ") DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin\n";
        Haruka.dbgOutput("EXECUTING QUERY: " + databaseConfiguringSQLQuery);
        try {
            Statement st = SQLConnectionsHandler.getLogDBConnection().createStatement();
            st.execute(databaseConfiguringSQLQuery);
            addMessage(message);
            Haruka.output("Database configured successfully!");
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            Haruka.crash(DatabaseManagement.class.getSimpleName(), "Could not configure database.");
        }
    }
}
