package utils.quotations;

import core.Haruka;
import core.MessageSender;
import org.telegram.telegrambots.api.objects.Message;
import utils.general.Crutches;
import utils.sql.DatabaseManagement;

import java.sql.Timestamp;


public class Quotations {
    public static void addQuotation(Message message) {
        if (message.getReplyToMessage() == null) {
            MessageSender.sendMessage(message.getChatId(), "You must reply to " +
                    "the message, you want to cite!");
            return;
        }
        String citationText = message.getReplyToMessage().getText().trim();
        String[] result = DatabaseManagement.addQuotationToQuotationsDB(message.getReplyToMessage().getFrom().getId(),
                Crutches.tgGetUser(message.getReplyToMessage()), citationText, new Timestamp(message.getReplyToMessage().getDate().longValue() * 1000L),
                Haruka.currentConfig.getCitationsSqlTableName());
        if (result[0].equals("FAIL")) {
            MessageSender.sendMessage(message.getChatId(), "Something is wrong:\n" + result[1]);
        } else MessageSender.sendReply(message.getChatId(), "Задокументировано!", message.getMessageId());
    }
    public static String getQuotationsByUser(Message message){
        return DatabaseManagement.getQuotationsByUser(message);
    }
}
