package core;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;
import utils.deleetify.Deleetify;
import utils.gameservermanagement.Management;
import utils.general.Crutches;
import utils.repeater.Repeater;
import utils.updateresolvers.UpdateResolver;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import utils.reposter.Reposter;

public class Haruka {
    public static Bot HarukaBot = null;
    public static String version = "1.1935.03(ver.29)";
    static String versionName = "Quotations update! ";
    public static Config currentConfig = new Config();
    private static long startTime;
    private static boolean debug;
    public static void main(String args[]) {
        startTime = System.currentTimeMillis();
        Haruka.output("Haruka v." + version + "\n" + versionName);
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        Reposter.init();
        ApiContextInitializer.init();
        HarukaBot = checkTokenAndRegisterBot();
        Management.initialize();
        Deleetify.init();
        output(strDate + ": Bot started.");
        MessageSender.sendMessage(270432410L, "Haruka v" + version + "\n" + versionName);
        Crutches.buildReport();
    }

    private static Bot checkTokenAndRegisterBot() {
    	 TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
         Bot haruka = new Bot(currentConfig.getBotToken());
         try {
             telegramBotsApi.registerBot(haruka);
             return haruka;
         } catch (TelegramApiRequestException e) {
             e.printStackTrace();
             Haruka.crash(Haruka.class.getSimpleName(), "Could not register bot.\n"
               		+ "Either you do not have internet connection or the bot token, you entered in config file, is wrong.");
         }
         return null;
    }
    
    public static void output(String inpText) {
        String text = "[" + (System.currentTimeMillis() - Haruka.startTime) / 1000 + "." + (System.currentTimeMillis() - Haruka.startTime) % 1000 + "] " + inpText;
        System.out.println(text);
    }

    public static void dbgOutput(String inpText){
        if(isDebug()){
            output("DEBUG: " + inpText);
        }
    }

    public static void crash(String crashClass, String message) {
    	try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	System.out.println("HARUKA v."+version+" EXITED WITH ERROR CODE!\n"
    			+ "Class: "+crashClass+"\n"
    					+ "Crash message: "+message);
    	System.exit(1);
    }
    
    public static void messageOutput(Message message) {
        Haruka.output(UpdateResolver.resolveMessages(message, true, true));
        Thread repeRepoThread = new Thread("Repeater and reposter") {
      public void run(){
          if(Haruka.currentConfig.isRepeater()) {
              Repeater.checkAndRepeat(message);
          }
        if(Haruka.currentConfig.isReposter() && message.hasPhoto()) {
            Reposter.checkAndRepost(message);
        }
      }
   };
        repeRepoThread.start();
    }

    static void restart(Message message) throws IOException {
        if (message.getChatId() != null) {
            Haruka.output("Restarting!");
            MessageSender.sendMessage(message.getChatId(), "Restarting!");
            Runtime.getRuntime().exec("java -jar Haru-chan.jar");
            System.exit(0);
        } else {
            Haruka.output("Restarting!");
            Runtime.getRuntime().exec("java -jar Haru-chan.jar");
            System.exit(0);
        }
    }

    public static long getStartTime() {
        return startTime;
    }

    public static boolean isDebug() {
        return debug;
    }

    static void toggleDebug() {
        Haruka.output("DEBUG TOGGLED");
        Haruka.debug = !Haruka.debug;
    }
}
